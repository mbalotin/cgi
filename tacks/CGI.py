from matplotlib import pyplot as plt
from matplotlib import animation as anim
from scipy.misc import imread
from Person import Person
from Position import Position
from Metrics import Metrics
import sys

try:
	folder =  sys.argv[1]
except Exception:
	print "E necessario informar o diretorio que deseja analisar como argumento da execucao."
	sys.exit()
print "Adicione a opcao -a para mostrar os frames lidos em um plot."
input_file = open('%sPaths_N.txt' % folder,'r')
colors = 'bgrcmyk'
forms=['s', '^']
persons = dict()

def animate(frame):
	plt.gcf().clear()
	try:
		plt.imshow(imread('%s%06d.jpg' % (folder, frame)))
	except IOError:
		return;
	for key in persons:
		for frm in range(frame):
			if frm in persons[key].path.keys():
				plt.plot(persons[key].path[frm].x, persons[key].path[frm].y, colors[key % 6]+forms[key/7])

def init():
	plt.gcf().clear()



index = 0
last_frame=0
# First loop, required to define the total frame count and the persons within the windows.
for line in input_file:
	persons[index] = Person();
	for coord in line.split("\t")[1].replace(')','').split('('):
		if coord:
			pos = coord.split(",")
			persons[index].add_point(pos[0], pos[1], pos[2])
			last_frame = max( int(pos[2]) , last_frame)
	index += 1

# Starts the input of the _D file in a matrix wher each row is a person, and e colum is a time frame
input_file = open('%sPaths_D.txt' % folder,'r')
gblMatriz=[[0 for y in range(last_frame+1)] for x in range(len(persons))]
index = 0
for line in input_file:
	if line[0] == '[':
		pixels_per_meter = int(line.replace('[','').replace(']',''))
	else:
		for coord in line.split("\t")[1].replace(')','').split('('):
			if coord:
				pos = coord.split(",")
				#print 'persons %d, max frames %d, current %s, persons %d' % (len(persons), last_frame, pos[2], index)
				gblMatriz[index][int(pos[2])] = Position(pos[0], pos[1])
		index += 1
print 'Loaded file'
print '    Frames: %d' % last_frame
print '    Persons: %d' % len(persons)
print '    Pixels per Meter: %d' % pixels_per_meter

Metrics.proxemic_distance(gblMatriz, pixels_per_meter)

# checks if -a options were informed
for param in sys.argv:
        if param == '-a':
                animation = anim.FuncAnimation(plt.figure(), animate, frames=last_frame, init_func=init)
                #plt.ylim(ymin=0)
                #plt.xlim(xmin=0)
                plt.show()
                break;

