from math import sqrt

class Position:
	def __init__(self, x, y):
		self.x = int(x)
		self.y = int(y)
	def distance(self, position, pixels_per_meter):
		# distance between two positions im cm
		distance = sqrt((self.x-position.x)**2) + ((self.y-position.y)**2) / pixels_per_meter
		if (distance <= 45):
			return 'INTIM'
		elif (distance <= 120):
			return 'PERSONAL'
		elif (distance <= 350):
			return 'SOCIAL'
		else:
			return 'PUBLIC'

