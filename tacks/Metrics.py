from Group import Group
class Metrics(object):
	# Determinate group formations, according to the proxemic communication parameter, defined by Edward T. Hall
	@staticmethod
	def proxemic_distance(matrix, pixels_per_meter):
		groups = []
		qtd_persons = len(matrix)
		qtd_frames = len(matrix[0])
		for frame in range(qtd_frames):
			for pivot_person in range(qtd_persons):
				pivot_position = matrix[pivot_person][frame]
				if (pivot_position != 0):
					for person in range(qtd_persons):
						person_position = matrix[person][frame]
						if (person_position != 0 and person_position > pivot_position):
							proxemics = pivot_position.distance(person_position, pixels_per_meter)
							if proxemics != 'PUBLIC':
								Metrics.__on_group(person, pivot_person, proxemics, groups)
		print 'Groups Found:'
		for group in groups:
			print 'Proxemic: %s, %s' % (group.proxemics, group.persons)
	@staticmethod
	def __on_group(person1, person2, proxemics, groups):
		for group in groups:
			if person1 in group.persons and person2 in group.persons:
				if (group.proxemics == proxemics):
					return
			elif person1 in group.persons:
				if group.proxemics == proxemics:
					group.persons.append(person2)
					return
			elif person2 in group.persons:
				if group.proxemics == proxemics:
					group.persons.append(person1)
					return
			
		groups.append(Group(person1, person2, proxemics))
