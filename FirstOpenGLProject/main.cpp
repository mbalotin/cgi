#include <stdlib.h>
#include <GL/glut.h>

float T=0;
// Fun��o callback de redesenho da janela de visualiza��o
void Desenha(void)
{
	// Limpa a janela de visualiza��o com a cor branca
	glClearColor(0,0,0,1);
	glClear(GL_COLOR_BUFFER_BIT);

	// Define a cor de desenho: azul
	glColor3f(0,0,1);

	// Desenha um tri�ngulo no centro da janela
	glBegin(GL_TRIANGLES);
	    glColor3f(1,1,1);
		glVertex3f(-0.5,-0.5,0);
		glVertex3f( 0.0, 0.5,0);
		glVertex3f( 0.5,-0.5,0);

	    glColor3f(0,0,0);

		glVertex3f(-0.47,-0.48,0);
		glVertex3f( 0.0, 0.45,0);
		glVertex3f( 0.47,-0.48,0);

	    glColor3f(1,1,1);

		glVertex3f(-0.24,0,0);
	    glColor3f(0,0,0);
		glVertex3f(0.22,0,0);
		glVertex3f(0.28,-0.1,0);
	glEnd();

	glColor3f(1,1,1);
	glBegin(GL_LINES);
		glVertex2f(-0.25,0);
		glVertex2f(-1.00,-0.2);
	glEnd();

	glColor3f(1,0,0);
	glBegin(GL_POLYGON);
		glVertex2f(0.25,0);
	glColor3f(0,1,0);
		glVertex2f(1,0+T);
	glColor3f(0,0,1);
		glVertex2f(1,-0.5+T);
	glColor3f(1,0,1);
		glVertex2f(0.31,-0.12);
    glEnd();
	//Executa os comandos OpenGL
	glFlush();
	glutPostRedisplay();
}

// Fun��o callback chamada para gerenciar eventos de teclas
void Teclado (unsigned char key, int x, int y)
{
	if (key == 27)
		exit(0);
}
void Teclas (int key, int x, int y)
{
	if (key == GLUT_KEY_DOWN)
        T-=0.05;
	if (key == GLUT_KEY_UP)
        T+=0.05;

}

// Fun��o respons�vel por inicializar par�metros e vari�veis
void Inicializa(void)
{
	// Define a janela de visualiza��o 2D
	glMatrixMode(GL_PROJECTION);
	gluOrtho2D(-1.0,1.0,-1.0,1.0);
	glMatrixMode(GL_MODELVIEW);
}

// Programa Principal
int main(void)
{
	int argc = 0;
	char *argv[] = { (char *)"gl", 0 };

	glutInit(&argc,argv);

// Define do modo de opera��o da GLUT
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);

	// Especifica o tamanho inicial em pixels da janela GLUT
	glutInitWindowSize(500,500);

	// Cria a janela passando como argumento o t�tulo da mesma
	glutCreateWindow("Desenho de um tri�ngulo em 2D");

	// Registra a fun��o callback de redesenho da janela de visualiza��o
	glutDisplayFunc(Desenha);

	// Registra a fun��o callback para tratamento das teclas ASCII
	glutKeyboardFunc (Teclado);

	glutSpecialFunc(Teclas);

	// Chama a fun��o respons�vel por fazer as inicializa��es
	Inicializa();

	// Inicia o processamento e aguarda intera��es do usu�rio
	glutMainLoop();

	return 0;
}
